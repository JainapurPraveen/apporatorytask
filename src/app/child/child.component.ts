import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit, OnChanges {
@Input() selectedUser;
  constructor() { }

  ngOnInit(): void { }
  ngOnChanges(): void {
    console.log('selectedUser', this.selectedUser);
  }

}
