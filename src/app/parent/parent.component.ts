import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {

  users: any [] ;
  selectedUser: any;
  constructor() { }

  ngOnInit(): void {
    this.users = [
      {
        id: 1,
        name: 'Praveen',
        detailes: {firstName: 'Praveen', lastname: 'jainapur', qalification: 'B.E', degreeScore: '8.15', age: 23}
      },
      {
        id: 2,
        name: 'sid',
        detailes: {firstName: 'sid', lastname: 'abc', qalification: 'B.E', degreeScore: '8.0', age: 27}
      },
      {
        id: 3,
        name: 'john',
        detailes: {firstName: 'john', lastname: 'sigha', qalification: 'B.E', degreeScore: '9.0', age: 22}
      },
      {
        id: 4,
        name: 'sunil',
        detailes: {firstName: 'sunil', lastname: 'hosr', qalification: 'Btech', degreeScore: '8.1', age: 29}
      },
      {
        id: 5,
        name: 'ravi',
        detailes: {firstName: 'ravi', lastname: 'udapudi', qalification: 'arts', degreeScore: '7.0', age: 33}
      },
    ];
  }

  showUserDetail(user): any {
    this.selectedUser = user;
  }
}
